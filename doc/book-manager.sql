create table user_info(
    id int auto_increment primary key comment '主键',
    username varchar(32) not null comment '用户名',
    password char(32) not null comment '密码',
    create_time datetime not null default current_timestamp comment '注册时间',
    update_time datetime default current_timestamp on update current_timestamp comment '最后修改时间',
    status int(1) not null default 0 comment '状态: 0-有效 1-无效'
);

create table book(
    id int auto_increment primary key comment '主键',
    name varchar(128) not null comment '书籍名称',
    isbn char(13) not null comment '书籍编号',
    press varchar(256) not null comment '出版社',
    cover varchar(1024) not null comment '封面',
    create_time datetime not null default current_timestamp comment '注册时间',
    update_time datetime default current_timestamp on update current_timestamp comment '最后修改时间',
    status int(1) not null default 0 comment '状态: 0-有效 1-无效'
);

create table borrow(
    id int auto_increment primary key comment '主键',
    book_id int not null comment '书籍ID',
    user_id int not null comment '用户ID',
    create_time datetime default current_timestamp comment '借阅时间',
    return_time datetime comment '归还时间',
    status int(1) not null comment '状态: 0-借阅 1-归还'
)

