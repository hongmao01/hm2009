package cn.jasonone.bean;

import java.util.Date;

public class Borrow implements PrimaryKey<Integer>{
	/**
	 * 主键
	 */
	private Integer id;
	
	/**
	 * 书籍ID
	 */
	private Integer bookId;
	
	/**
	 * 用户ID
	 */
	private Integer userId;
	
	/**
	 * 借阅时间
	 */
	private Date createTime;
	
	/**
	 * 归还时间
	 */
	private Date returnTime;
	
	/**
	 * 状态: 0-借阅 1-归还
	 */
	private Integer status;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getBookId() {
		return bookId;
	}
	
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	
	public Integer getUserId() {
		return userId;
	}
	
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getReturnTime() {
		return returnTime;
	}
	
	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}
	
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
}

