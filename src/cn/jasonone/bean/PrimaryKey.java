package cn.jasonone.bean;

/**
 * @author 许杰
 */
public interface PrimaryKey<ID> {
	ID getId();
	void setId(ID id);
}
