package cn.jasonone.bean.vo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;

/**
 * value-objct
 * @author 许杰
 */
public class HttpResult extends HashMap<String,Object> {
	
	private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	public HttpResult() {
		setCode(0);
	}
	
	public void setCode(int code){
		this.put("code", code);
	}
	
	public int getCode(){
		if(!this.containsKey("code")){
			return 0;
		}
		return (int) this.get("code");
	}
	
	public void setMessage(String message){
		this.put("message", message);
	}
	
	public String getMessage(){
		return (String) this.get("message");
	}
	
	public void setData(Object data){
		this.put("data",data);
	}
	
	public <T> T getData(){
		return (T) this.get("data");
	}
	
	@Override
	public String toString() {
		return gson.toJson(this);
	}
}
