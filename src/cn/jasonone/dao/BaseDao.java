package cn.jasonone.dao;

import cn.jasonone.bean.PrimaryKey;
import cn.jasonone.bean.UserInfo;
import cn.jasonone.util.ClassUtil;
import cn.jasonone.util.DBUtil;
import cn.jasonone.util.GeneratedKey;
import cn.jasonone.util.ObjectUtil;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 实现基本增删改查的功能
 *
 * @author 许杰
 */
public abstract class BaseDao<E extends PrimaryKey<Integer>, ID> {
	
	public int insert(E e) {
		Objects.requireNonNull(e, "新增的对象不能为null");
		Class<? extends PrimaryKey> type = e.getClass();
		List<String> fieldNames = ClassUtil.getFiledNames(type);
		List<Field> notNullFields = new ArrayList<>();
		List<Object> params = new ArrayList<>();
		for (String fieldName : fieldNames) {
			Object value = ClassUtil.getFiledValue(e, fieldName);
			if (value != null) {
				Field field = ClassUtil.getField(type, fieldName);
				notNullFields.add(field);
				params.add(value);
			}
		}
		if (notNullFields.isEmpty()) {
			throw new RuntimeException(new SQLException("至少新增一个字段"));
		}
		
		StringBuilder sql = new StringBuilder("insert into ");
		
		sql.append(ObjectUtil.toColumnName(type.getSimpleName()));
		sql.append("(");
		for (Field field : notNullFields) {
			sql.append(ObjectUtil.toColumnName(field.getName())).append(",");
		}
		
		sql.replace(sql.length() - 1, sql.length(), ") values (");
		for (Field field : notNullFields) {
			sql.append("?,");
		}
		sql.replace(sql.length() - 1, sql.length(), ")");
		
		return DBUtil.update(sql.toString(), new GeneratedKey() {
			@Override
			public void keys(Integer id) {
				e.setId(id);
			}
		}, params.toArray());
	}
	
	public int updateById(E e) {
		Objects.requireNonNull(e, "修改的对象不能为null");
		Class<? extends PrimaryKey> type = e.getClass();
		List<String> fieldNames = ClassUtil.getFiledNames(type);
		List<Field> notNullFields = new ArrayList<>();
		List<Object> params = new ArrayList<>();
		for (String fieldName : fieldNames) {
			Object value = ClassUtil.getFiledValue(e, fieldName);
			if (value != null && !fieldName.equals("id")) {
				Field field = ClassUtil.getField(type, fieldName);
				notNullFields.add(field);
				params.add(value);
			}
		}
		if (notNullFields.isEmpty()) {
			throw new RuntimeException(new SQLException("至少新增一个字段"));
		}
		
		StringBuilder sql = new StringBuilder("update ");
		
		sql.append(ObjectUtil.toColumnName(type.getSimpleName()));
		sql.append(" set ");
		for (Field field : notNullFields) {
			sql.append(ObjectUtil.toColumnName(field.getName())).append("=? ,");
		}
		
		sql.replace(sql.length() - 1, sql.length(), " where id = ?");
		params.add(e.getId());
		return DBUtil.update(sql.toString(), null, params.toArray());
	}
	
	public int update(String sql, List<Object> params) {
		return update(sql, null, params.toArray());
	}
	
	public int update(String sql, Object... params) {
		return DBUtil.update(sql, null, params);
	}
	
	public List<E> select(String sql, Class<E> type, List<Object> params) {
		return select(sql, type, params.toArray());
	}
	
	public List<E> select(String sql, Class<E> type, Object... params) {
		return DBUtil.select(sql, type, params);
	}
	
	
	public E selectOne(String sql, Class<E> type, Object... params) {
		return DBUtil.selectOne(sql, type, params);
	}
	
	public int count(String sql, Object... params) {
		return DBUtil.count(sql, params);
	}
}
