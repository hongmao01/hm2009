package cn.jasonone.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author 许杰
 */
public class CharsetEncodingFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	
	}
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) servletRequest;
		servletRequest.setCharacterEncoding("UTF-8");
		String uri = req.getRequestURI().substring(req.getContextPath().length());
		if(!uri.matches("/assets/.*")){
			servletResponse.setContentType("text/html;charset=UTF-8");
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}
	
	@Override
	public void destroy() {
	
	}
}
