package cn.jasonone.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author 许杰
 */
public class LoginFilter implements Filter {
	private List<String> urls = new ArrayList<>();
	@Override
	public void init(FilterConfig config) throws ServletException {
		String urls = config.getInitParameter("urls");
		if(urls != null && !urls.isEmpty()){
			String[] split = urls.split("\n");
			for (String s : split) {
				this.urls.add(s.trim());
			}
		}
		System.out.println("默认放行的URL:"+this.urls);
	}
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest req= (HttpServletRequest) servletRequest;
		HttpServletResponse resp= (HttpServletResponse) servletResponse;
		
		String uri = req.getRequestURI().substring(req.getContextPath().length());
		
		for (String url : urls) {
			if(uri.matches(url)){
				filterChain.doFilter(req, resp);
				return;
			}
		}
		
		if(req.getSession().getAttribute("LOGIN_INFO") != null){
			filterChain.doFilter(req, resp);
		}else{
			resp.sendRedirect(req.getContextPath()+"/login.jsp");
		}
	}
	
	@Override
	public void destroy() {
	
	}
}
