package cn.jasonone.listener;

import cn.jasonone.util.DBUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author 许杰
 */
public class DBListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		DBUtil.init("com.mysql.cj.jdbc.Driver", "jdbc:mysql://localhost:3306/2009?serverTimezone=GMT%2b8", "root", "root");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	
	}
}
