package cn.jasonone.service;

import cn.jasonone.bean.PrimaryKey;
import cn.jasonone.dao.BaseDao;
import cn.jasonone.util.DBUtil;
import cn.jasonone.util.ObjectUtil;

import java.util.List;

/**
 * @author 许杰
 */
public interface BaseService<DAO extends BaseDao<E, ID>,E extends PrimaryKey<Integer>,ID> {
	DAO getBaseDao();
	
	Class<E> getType();
	
	default int save(E e){
		if(e.getId() == null){
			return getBaseDao().insert(e);
		}else{
			return getBaseDao().updateById(e);
		}
	}
	
	default List<E> findAll(E e,Integer page,Integer pageSize){
		return (List<E>)getBaseDao().select("select * from "+ObjectUtil.toColumnName(getType().getSimpleName()), getType());
	}
	
	default int deleteById(Integer id){
		return getBaseDao().update("delete from "+ObjectUtil.toColumnName(getType().getSimpleName())+" where id = ?", id);
	}
	
	default E findById(Integer id){
		return getBaseDao().selectOne("select * from "+ObjectUtil.toColumnName(getType().getSimpleName())+" where id = ?", getType(), id);
	}
	
	default int deleteByIds(Integer ... ids){
		if(ids.length < 1){
			return 0;
		}
		StringBuilder sql = new StringBuilder("delete from "+ObjectUtil.toColumnName(getType().getSimpleName())+" where id in (");
		for (Integer id : ids) {
			sql.append("?,");
		}
		sql.replace(sql.length()-1, sql.length(), ")");
		return getBaseDao().update(sql.toString(), ids);
	}
	
	default int count(E e){
		return 0;
	}
}
