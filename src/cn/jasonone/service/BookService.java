package cn.jasonone.service;

import cn.jasonone.bean.Book;
import cn.jasonone.dao.BookDao;

/**
 * @author 许杰
 */
public interface BookService extends BaseService<BookDao, Book,Integer> {
}
