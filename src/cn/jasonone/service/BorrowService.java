package cn.jasonone.service;

import cn.jasonone.bean.Borrow;
import cn.jasonone.dao.BorrowDao;

/**
 * @author 许杰
 */
public interface BorrowService extends BaseService<BorrowDao, Borrow,Integer> {
}
