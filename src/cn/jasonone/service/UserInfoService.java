package cn.jasonone.service;

import cn.jasonone.bean.UserInfo;
import cn.jasonone.dao.UserInfoDao;

/**
 * @author 许杰
 */
public interface UserInfoService extends BaseService<UserInfoDao, UserInfo,Integer> {
	
	UserInfo login(UserInfo user);
	
	void register(UserInfo user);

}
