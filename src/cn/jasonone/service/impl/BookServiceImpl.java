package cn.jasonone.service.impl;

import cn.jasonone.bean.Book;
import cn.jasonone.dao.BookDao;
import cn.jasonone.service.BookService;
import cn.jasonone.util.ObjectUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 许杰
 */
public class BookServiceImpl implements BookService {
	private BookDao dao = new BookDao();
	@Override
	public BookDao getBaseDao() {
		return dao;
	}
	
	@Override
	public Class<Book> getType() {
		return Book.class;
	}
	
	@Override
	public List<Book> findAll(Book book,Integer page,Integer pageSize) {
		StringBuilder sql = new StringBuilder("select * from book where 1=1 ");
		List<Object> params = new ArrayList<>();
		if(book != null){
			if(!ObjectUtil.isEmpty(book.getName())){
				sql.append(" and name like concat('%',?,'%')");
				params.add(book.getName());
			}
			if(!ObjectUtil.isEmpty(book.getIsbn())){
				sql.append(" and isbn like concat('%',?,'%')");
				params.add(book.getIsbn());
			}
			if(!ObjectUtil.isEmpty(book.getPress())){
				sql.append(" and press like concat('%',?,'%')");
				params.add(book.getPress());
			}
			if(!ObjectUtil.isEmpty(book.getStatus())){
				sql.append(" and press status = ?");
				params.add(book.getStatus());
			}
		}
		if(page != null && pageSize != null){
			sql.append(" limit ?,?");
			params.add((page-1)*pageSize);
			params.add(pageSize);
		}
		return dao.select(sql.toString(), getType(), params);
	}
	
	@Override
	public int count(Book book) {
		StringBuilder sql = new StringBuilder("select count(1) from book where 1=1 ");
		List<Object> params = new ArrayList<>();
		if(book != null){
			if(!ObjectUtil.isEmpty(book.getName())){
				sql.append(" and name like concat('%',?,'%')");
				params.add(book.getName());
			}
			if(!ObjectUtil.isEmpty(book.getIsbn())){
				sql.append(" and isbn like concat('%',?,'%')");
				params.add(book.getIsbn());
			}
			if(!ObjectUtil.isEmpty(book.getPress())){
				sql.append(" and press like concat('%',?,'%')");
				params.add(book.getPress());
			}
			if(!ObjectUtil.isEmpty(book.getStatus())){
				sql.append(" and press status = ?");
				params.add(book.getStatus());
			}
		}
		return dao.count(sql.toString(), params.toArray());
	}
}
