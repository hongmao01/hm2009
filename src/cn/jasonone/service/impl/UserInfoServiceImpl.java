package cn.jasonone.service.impl;

import cn.jasonone.bean.UserInfo;
import cn.jasonone.dao.UserInfoDao;
import cn.jasonone.service.UserInfoService;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author 许杰
 */
public class UserInfoServiceImpl implements UserInfoService {
	private UserInfoDao dao = new UserInfoDao();
	@Override
	public UserInfoDao getBaseDao() {
		return dao;
	}
	
	@Override
	public Class<UserInfo> getType() {
		return UserInfo.class;
	}
	
	
	@Override
	public UserInfo login(UserInfo user) {
		return dao.selectOne("select * from user_info where username = ? and password = md5(?)", UserInfo.class, user.getUsername(),user.getPassword());
	}
	
	@Override
	public void register(UserInfo user) {
		String password = DigestUtils.md5Hex(user.getPassword());
		user.setPassword(password);
		save(user);
	}
}
