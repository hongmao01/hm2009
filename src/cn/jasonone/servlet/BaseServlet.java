package cn.jasonone.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 许杰
 */
public abstract class BaseServlet<E> extends HttpServlet {

	public abstract E parseBean(HttpServletRequest req);
	
	public String getPath(HttpServletRequest req){
		return req.getRequestURI().substring(req.getContextPath().length());
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// /book/add/fdsa
		// method=方法名
		String m = req.getParameter("m");
		if(m == null || m.isEmpty()){
			resp.sendError(404, "找不到资源:"+getPath(req));
			return;
		}
		try {
			Method method = this.getClass().getMethod(m, HttpServletRequest.class, HttpServletResponse.class);
			method.invoke(this, req,resp);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			resp.sendError(404, "找不到资源:"+getPath(req));
		} catch (IllegalAccessException|InvocationTargetException e) {
			e.printStackTrace();
			resp.sendError(500, "服务器内部错误:"+e.getMessage());
		}
		
	}
}
