package cn.jasonone.servlet;

import cn.jasonone.bean.Book;
import cn.jasonone.bean.vo.HttpResult;
import cn.jasonone.service.BookService;
import cn.jasonone.service.impl.BookServiceImpl;
import cn.jasonone.util.ObjectUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 许杰
 */
public class BookServlet extends BaseServlet<Book>{
	private BookService bookService = new BookServiceImpl();
	@Override
	public Book parseBean(HttpServletRequest req) {
		Book book = new Book();
		book.setId(ObjectUtil.parseInt(req.getParameter("id")));
		book.setIsbn(req.getParameter("isbn"));
		book.setName(req.getParameter("name"));
		book.setPress(req.getParameter("press"));
		book.setStatus(ObjectUtil.parseInt(req.getParameter("status")));
		book.setCover(req.getParameter("cover"));
		return book;
	}
	
	public void save(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		Book book = parseBean(req);
		bookService.save(book);
		HttpResult hr = new HttpResult();
		hr.setMessage("保存成功");
		resp.getWriter().println(hr.toString());
	}

	public void findAll(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		Integer limit = ObjectUtil.parseInt(req.getParameter("limit"));
		Integer page = ObjectUtil.parseInt(req.getParameter("page"));
		Book book = parseBean(req);
		List<Book> list = bookService.findAll(book,page,limit);
		int count = bookService.count(book);
		HttpResult hr = new HttpResult();
		hr.setData(list);
		hr.put("count", count);
		resp.getWriter().println(hr.toString());
	}
	
	
	
	public void deleteByIds(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		String[] ids = req.getParameterValues("id");
		HttpResult hr = new HttpResult();
		if(!ObjectUtil.isEmpty(ids)){
			Integer[] ids2 = new Integer[ids.length];
			for (int i = 0; i < ids.length; i++) {
				ids2[i]=ObjectUtil.parseInt(ids[i]);
			}
			bookService.deleteByIds(ids2);
			hr.setMessage("删除成功");
		}else{
			hr.setCode(20002);
			hr.setMessage("请指定需要删除的书籍");
		}
		resp.getWriter().println(hr.toString());
	}
	public void findById(HttpServletRequest req, HttpServletResponse resp) throws IOException{

		Book book = parseBean(req);
		book = bookService.findById(book.getId());
		
		HttpResult hr = new HttpResult();
		if(book == null){
			hr.setCode(20001);
			hr.setMessage("书籍不存在");
		}else{
			hr.setData(book);
		}
		resp.getWriter().println(hr.toString());
	}
	
	
}
