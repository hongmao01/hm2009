package cn.jasonone.servlet;

import cn.jasonone.bean.vo.HttpResult;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 许杰
 */
public class FileUploadServlet extends HttpServlet {
	private DiskFileItemFactory factory = new DiskFileItemFactory();
	private ServletFileUpload fileUpload;

	private File imagesFile = null;
	@Override
	public void init() throws ServletException {
		ServletContext application = this.getServletContext();
		String imagePath = application.getRealPath("/images");
		imagesFile = new File(imagePath);
		if(!imagesFile.exists()){
			imagesFile.mkdirs();
		}
		factory.setSizeThreshold(100<<20);
		fileUpload = new ServletFileUpload(factory);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			List<FileItem> fileItems = fileUpload.parseRequest(req);
			List<String> urls = new ArrayList<>();
			
			for (FileItem fileItem : fileItems) {
				if(!fileItem.isFormField()){
					InputStream is = fileItem.getInputStream();
					String fileName = DigestUtils.md5Hex(is);
					try {
						String ext = fileItem.getName().substring(fileItem.getName().lastIndexOf("."));
						fileItem.write(new File(imagesFile, fileName+ext));
						urls.add("/images/"+fileName+ext);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			HttpResult hr = new HttpResult();
			hr.setData(urls);
			hr.setMessage("上传成功");
			resp.getWriter().println(hr.toString());
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}
}
