package cn.jasonone.servlet;

import cn.jasonone.bean.UserInfo;
import cn.jasonone.bean.vo.HttpResult;
import cn.jasonone.service.UserInfoService;
import cn.jasonone.service.impl.UserInfoServiceImpl;
import cn.jasonone.util.ObjectUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 许杰
 */
public class UserInfoServlet extends BaseServlet<UserInfo>{
	private UserInfoService userInfoService = new UserInfoServiceImpl();
	@Override
	public UserInfo parseBean(HttpServletRequest req) {
		UserInfo userInfo = new UserInfo();
		userInfo.setUsername(req.getParameter("username"));
		userInfo.setPassword(req.getParameter("password"));
		userInfo.setId(ObjectUtil.parseInt(req.getParameter("id")));
		userInfo.setStatus(ObjectUtil.parseInt(req.getParameter("status")));
		return userInfo;
	}

	public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		UserInfo userInfo = parseBean(req);
		userInfo = userInfoService.login(userInfo);
		HttpResult hr = new HttpResult();
		
		if(userInfo != null){
			req.getSession().setAttribute("LOGIN_INFO",userInfo);
			hr.setMessage("登录成功");
		}else{
			hr.setCode(10001);
			hr.setMessage("用户名或密码错误");
		}
		resp.getWriter().append(hr.toString());
	}

	public void register(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		userInfoService.register(parseBean(req));
		HttpResult hr = new HttpResult();
		hr.setMessage("注册用户成功");
		resp.getWriter().println(hr.toString());
	}
	
	public void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		req.getSession().invalidate();
		resp.sendRedirect(req.getContextPath()+"/login.jsp");
	}
}
