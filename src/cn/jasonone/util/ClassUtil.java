package cn.jasonone.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author 许杰
 */
public class ClassUtil {
	// 获取类的所有属性列表
	
	public static List<String> getFiledNames(Class<?> type) {
		List<String> fieldNames = new ArrayList<>();
		Field[] fields = type.getDeclaredFields();
		if (fields != null) {
			for (Field field : fields) {
				fieldNames.add(field.getName());
			}
		}
		Class<?> superclass = type.getSuperclass();
		if (superclass != null) {
			fieldNames.addAll(getFiledNames(superclass));
		}
		return fieldNames;
	}
	
	/**
	 * 检测指定类是否有指定名称的属性
	 *
	 * @param type
	 * @param fieldName
	 * @return
	 */
	public static boolean isField(Class<?> type, String fieldName) {
		List<String> filedNames = getFiledNames(type);
		return filedNames.contains(fieldName);
	}
	
	public static boolean isField(Object obj, String fieldName) {
		return isField(obj.getClass(), fieldName);
	}
	
	public static Field getField(Class<?> type, String fieldName) {
		if (isField(type, fieldName)) {
			try {
				return type.getDeclaredField(fieldName);
			} catch (NoSuchFieldException e) {
				throw new RuntimeException(e);
			}
		}
		return null;
	}
	
	// 获取指定实例的属性值
	public static <T> T getFiledValue(Object obj, String fieldName) {
		Objects.requireNonNull(obj, "实例不能为null");
		if (isField(obj, fieldName)) {
			Field field = getField(obj.getClass(), fieldName);
			field.setAccessible(true);
			try {
				return (T) field.get(obj);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		return null;
	}
	
	// 设置指定实例的属性值
	public static void getFiledValue(Object obj, String fieldName, Object value) {
		Objects.requireNonNull(obj, "实例不能为null");
		if (isField(obj, fieldName)) {
			Field field = getField(obj.getClass(), fieldName);
			field.setAccessible(true);
			try {
				field.set(obj, value);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
