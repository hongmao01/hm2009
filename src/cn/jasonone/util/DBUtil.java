package cn.jasonone.util;

import cn.jasonone.bean.PrimaryKey;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * @author 许杰
 */
public class DBUtil {
	private static String url;
	private static String username;
	private static String password;
	
	private static ThreadLocal<Connection> connectionThreadLocal = new ThreadLocal<>();
	
	public static void init(String driverClassName, String url, String username, String password) {
		DBUtil.url = url;
		DBUtil.username = username;
		DBUtil.password = password;
		try {
			Class.forName(driverClassName);
			System.out.println("初始化DBUtil:"+url+","+username+","+password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	private static Connection getConnection() throws SQLException {
		Connection connection = connectionThreadLocal.get();
		if (connection == null) {
			return DriverManager.getConnection(url, username, password);
		} else {
			return connection;
		}
	}
	
	/**
	 * 是否开启事务
	 *
	 * @return
	 */
	public static boolean isTransaction() {
		return connectionThreadLocal.get() != null;
	}
	
	/**
	 * 开启事务
	 */
	public static void beginTransaction() {
		try {
			Connection connection = getConnection();
			connection.setAutoCommit(false);
			connectionThreadLocal.set(connection);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		
	}
	
	/**
	 * 提交事务
	 */
	public static void commit() {
		try (Connection connection = getConnection()) {
			connection.commit();
			connectionThreadLocal.remove();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		
	}
	
	/**
	 * 回滚事务
	 */
	public static void rollback() {
		try (Connection connection = getConnection()) {
			connection.rollback();
			connectionThreadLocal.remove();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
	
	private static void close(AutoCloseable closeable) {
		if (closeable != null) {
			if (closeable instanceof Connection && isTransaction()) {
				return;
			}
			try {
				closeable.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private static String toField(String columnName){
		String fieldName = "";
		final char[] chars = columnName.toLowerCase().toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if(chars[i] == '_'){
				chars[++i]=Character.toUpperCase(chars[i]);
			}
			fieldName+=chars[i];
		}
		return fieldName;
	}
	
	private static String toColumnName(String fieldName){
		String columnName = "";
		final char[] chars = fieldName.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if(i> 0 && Character.isUpperCase(chars[i])){
				columnName+="_";
			}
			columnName+=chars[i];
		}
		return columnName.toUpperCase();
	}
	
	private static <T> T toBean(ResultSet rs,Map<String, Field> fields, Class<T> type) throws IllegalAccessException, InstantiationException, SQLException {
		T t = type.newInstance();
		for (Map.Entry<String, Field> entry : fields.entrySet()) {
			Field field = entry.getValue();
			field.setAccessible(true);
			field.set(t, rs.getObject(entry.getKey()));
		}
		return t;
	}
	
	public static int count(String sql,Object...params){
		try (Connection connection = getConnection()) {
			System.out.println("SQL:\t"+sql);
			System.out.println("PARAMS:\t"+Arrays.toString(params));
			try (PreparedStatement ps = connection.prepareStatement(sql)) {
				if (params != null && params.length > 0) {
					for (int i = 0; i < params.length; i++) {
						ps.setObject(i + 1, params[i]);
					}
				}
				try (ResultSet rs = ps.executeQuery()) {
					if(rs.next()){
						return rs.getInt(1);
					}
				}
			}
			
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return 0;
	}
	
	public static int update(String sql, GeneratedKey generatedKey, Object... params) {
		Connection connection = null;
		try {
			connection = getConnection();
			System.out.println("SQL:\t"+sql);
			System.out.println("PARAMS:\t"+Arrays.toString(params));
			try (PreparedStatement ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
				if (params != null && params.length > 0) {
					for (int i = 0; i < params.length; i++) {
						ps.setObject(i + 1, params[i]);
					}
				}
				int rows = ps.executeUpdate();
				try (ResultSet rs = ps.getGeneratedKeys()) {
					if (rs.next() && generatedKey != null) {
						generatedKey.keys(rs.getInt(1));
					}
				}
				return rows;
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			close(connection);
		}
		
		
		return 0;
	}
	
	public static <T extends PrimaryKey<Integer>> List<T> select(String sql, Class<T> type, Object... params) {
		try (Connection connection = getConnection()) {
			System.out.println("SQL:\t"+sql);
			System.out.println("PARAMS:\t"+Arrays.toString(params));
			try (PreparedStatement ps = connection.prepareStatement(sql)) {
				if (params != null && params.length > 0) {
					for (int i = 0; i < params.length; i++) {
						ps.setObject(i + 1, params[i]);
					}
				}
				try (ResultSet rs = ps.executeQuery()) {
					final ResultSetMetaData md = rs.getMetaData();
					Map<String, Field> fields = new HashMap<>();
					for (int i = 0; i < md.getColumnCount(); i++) {
						String columnLabel = md.getColumnLabel(i + 1);
						try {
							final Field field = type.getDeclaredField(toField(columnLabel));
							fields.put(columnLabel,field);
						} catch (NoSuchFieldException e) {
						
						}
					}
					List<T> list = new ArrayList<>();
					while (rs.next()) {
						try {
							list.add(toBean(rs,fields, type));
						} catch (IllegalAccessException|InstantiationException e) {
							e.printStackTrace();
						}
					}
					return list.isEmpty() ? Collections.emptyList() : list;
				}
			}
			
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		
		
		return Collections.emptyList();
	}
	
	public static <T extends PrimaryKey<Integer>> T selectOne(String sql, Class<T> type, Object... params) {
		List<T> list = select(sql, type, params);
		if (list.size() == 1) {
			return list.get(0);
		} else if (list.size() > 1) {
			throw new RuntimeException(new SQLException("应该只有一行记录,但是返回了[" + list.size() + "]条记录"));
		}
		return null;
	}
}
