package cn.jasonone.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

/**
 * @author 许杰
 */
public class ObjectUtil {
	
	public static Integer parseInt(String number) {
		if (number != null && number.matches("\\d+")) {
			return Integer.valueOf(number);
		}
		return null;
	}
	
	public static String toColumnName(String name) {
		String columnName = "";
		char[] chars = name.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (i > 0) {
				if (Character.isUpperCase(chars[i])) {
					columnName += "_";
				}
				
			}
			columnName += chars[i];
		}
		return columnName.toUpperCase();
	}
	
	public static String toCamelCase(String columnName) {
		String camelCase = "";
		char[] chars = columnName.toLowerCase().toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == '_') {
				chars[++i] = Character.toUpperCase(chars[i]);
			}
			camelCase += chars[i];
		}
		return camelCase;
	}
	
	public static boolean isEmpty(Object obj){
		if(obj == null) {
			return true;
		}
		if(obj instanceof String){
			return ((String) obj).isEmpty();
		}
		if(obj instanceof Collection){
			return ((Collection<?>) obj).isEmpty();
		}
		if(obj instanceof Map){
			return ((Map<?, ?>) obj).isEmpty();
		}
		if(obj.getClass().isArray() && Array.getLength(obj)==0){
			return true;
		}
		return false;
	}
}
