<%--
  Created by IntelliJ IDEA.
  User: 许杰
  Date: 2021/1/15
  Time: 12:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="/layui.jsp"/>
</head>
<body>
<form class="layui-form">
    <div class="layui-inline">
        <label class="layui-form-label">书籍名称:</label>
        <div class="layui-input-inline">
            <input type="text" name="name"  placeholder="请输入书籍名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">ISBN:</label>
        <div class="layui-input-inline">
            <input type="text" name="isbn"  placeholder="请输入ISBN" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">状态:</label>
        <div class="layui-input-inline">
            <input type="radio" name="status" checked title="所有" value="">
            <input type="radio" name="status" title="有效" value="0">
            <input type="radio" name="status" title="无效" value="1">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">出版社</label>
        <div class="layui-input-inline">
            <input type="text" name="press" placeholder="请输入出版社" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <div class="layui-input-inline">
            <button class="layui-btn" lay-submit lay-filter="query">
                <i class="layui-icon layui-icon-search"></i>
            </button>
        </div>
    </div>
</form>
    <table lay-filter="table-data" id="table-data"></table>
    <script type="text/html" id="toolbar">
        <a class="layui-btn " lay-event="add">新增</a>
        <a class="layui-btn layui-btn-danger " lay-event="del">批量删除</a>
    </script>
    <script id="cover" type="text/html">
        <img src="{{d.cover}}">
    </script>
    <script id="action" type="text/html">
        <a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="update"><i class="layui-icon layui-icon-edit"></i></a>
        <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del"><i class="layui-icon layui-icon-delete"></i></a>
    </script>
    <script>
        layui.use(['table','layer','form','jquery'], function(){
            let {table,layer,form,$} = layui;

            table.render({
                id:'table-data'
                ,elem: '#table-data'
                ,url:'${pageContext.request.contextPath}/book?m=findAll'
                // ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                ,toolbar:"#toolbar"
                ,cols: [[
                    { type:'checkbox'}
                    ,{field:'id',  title: 'ID'}
                    ,{field:'name',  title: '书籍名称'}
                    ,{field:'isbn',  title: 'ISBN'}
                    ,{field:'press',  title: '出版社'}
                    ,{field:'cover',  title: '封面',templet:"#cover"}
                    ,{field:'createTime',  title: '注册时间', sort: true}
                    ,{field:'updateTime',  title: '修改时间'}
                    ,{field:'status', title: '状态',templet:function (d) {
                            if(d.status == 0){
                                return '<span class="layui-badge layui-bg-blue">有效</span>';
                            }else{
                                return '<span class="layui-badge ">无效</span>'
                            }
                        }}
                    ,{title:'操作',fiexd:'right',templet: "#action"}
                    ]]
            });
            table.on("toolbar(table-data)",e=>{
                if(e.event == 'add'){
                    layer.open({
                        type:2,
                        area:["400px","500px"],
                        content:'${pageContext.request.contextPath}/books/save.jsp'
                    })
                }else if(e.event == 'del'){
                    layer.confirm("是否删除选中项?",index=>{
                        layer.close(index);
                        let i = layer.load(2,{shade:0.3})

                        var checkStatus = table.checkStatus('table-data');
                        // id=1&id=2&id=3
                        let queryString = checkStatus.data.map(book=>"id="+book.id).join("&");

                        $.get("${pageContext.request.contextPath}/book?m=deleteByIds&"+queryString,function (rs) {
                            layer.close(i);
                            if(rs.code == 0){
                                layer.msg("删除成功",{icon:6});
                                table.reload("table-data");
                            }else{
                                layer.msg("删除失败",{icon:5});
                            }

                        },'json');
                });
            }});
            table.on("tool(table-data)",e=>{
                if(e.event == 'del'){
                    layer.confirm("是否删除["+e.data.name+"]书籍?",function (index){
                        layer.close(index);
                        let i = layer.load(2,{shade:0.3});
                        $.get("${pageContext.request.contextPath}/book?m=deleteByIds&id="+e.data.id,function (rs) {
                            layer.close(i);
                            if(rs.code == 0){
                                layer.msg("删除成功",{icon:6});
                                e.del();
                            }else{
                                layer.msg("删除失败",{icon:5});
                            }

                        },'json');



                    });
                }else{
                    layer.open({
                        type:2,
                        title:'修改书籍信息',
                        area:["400px","500px"],
                        content:'${pageContext.request.contextPath}/books/save.jsp?id='+e.data.id
                    })
                }
            })
            form.on("submit(query)",e=>{
                table.reload('table-data',{
                    where:e.field
                })
                return false;
            })
        });
    </script>
</body>
</html>
