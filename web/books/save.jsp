<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="/layui.jsp"></jsp:include>
</head>
<body>

<form class="layui-form" lay-filter="saveForm">
    <input type="hidden" name="id">
    <input type="hidden" lay-verify="required" lay-reqText="封面不能为空" name="cover">
    <div class="layui-form-item">
        <label class="layui-form-label">书籍名称:</label>
        <div class="layui-input-block">
            <input type="text" name="name" lay-verify="required" lay-reqText="书籍名称不能为空"   placeholder="请输入书籍名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">ISBN:</label>
        <div class="layui-input-block">
            <input type="text" name="isbn" lay-verify="required" lay-reqText="ISBN不能为空" placeholder="请输入ISBN" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">出版社</label>
        <div class="layui-input-block">
            <input type="text" name="press" lay-verify="required" lay-reqText="出版社不能为空" placeholder="请输入出版社" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">封面:</label>
        <div class="layui-input-block">
            <div class="layui-upload-drag" id="cover">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
                <div class="layui-hide" id="preView">
                    <hr>
                    <img src="" alt="上传成功后渲染" style="max-width: 196px">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态:</label>
        <div class="layui-input-block">
            <input type="checkbox" name="status" value="1" lay-skin="switch" lay-text="有效|无效" checked>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="save">
                保存
            </button>
        </div>
    </div>
</form>
<script>
    layui.use(['upload','jquery','element','form'],function () {
        let {upload,$,element,form} = layui;

        let id = '${param.id}';

        if(id){
            $.get('${pageContext.request.contextPath}/book?m=findById&id='+id,function (rs) {
                if(rs.code == 0){
                    if(rs.data.status == 1){
                        rs.data.status=0;
                    }else{
                        rs.data.status=1;
                    }
                    form.val("saveForm",rs.data);
                }else{
                    layer.msg(rs.message,{icon:5});
                }
            },'json');
        }


        upload.render({
            elem: '#cover'
            ,url: '${pageContext.request.contextPath}/fileUpload' //改成您自己的上传接口
            ,done: function(res){
                layer.msg('上传成功');
                $('#preView').removeClass('layui-hide').find('img').attr('src', res.data[0]);
                form.val("saveForm",{cover:res.data[0]})
            }
        });

        form.on("submit(save)",e=>{
            if(e.field.status == '1'){
                e.field.status=0;
            }else{
                e.field.status=1;
            }
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath}/book?m=save',
                data:e.field,
                dataType:'json',
                success(rs){
                    if(rs.code == 0){
                        if(parent){
                            let frameIndex = parent.layer.getFrameIndex(window.name);
                            parent.layer.msg(rs.message);
                            parent.layui.table.reload('table-data');
                            parent.layer.close(frameIndex);
                        }else{
                            layer.msg(rs.message);
                        }
                    }else{
                        layer.msg(rs.message,{icon:5});
                    }
                }
            })
            return false;
        })
    })
</script>
</body>
</html>
