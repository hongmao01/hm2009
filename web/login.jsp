<%--
  Created by IntelliJ IDEA.
  User: 许杰
  Date: 2021/1/14
  Time: 11:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <jsp:include page="layui.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/login.css">
</head>
<body class="center">
    <div class="lay-row ">
        <div class="layui-col-md4 layui-col-md-offset4">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">用户名:</label>
                    <div class="layui-input-block">
                        <input type="text" name="username" lay-verify="required" lay-reqText="用户名不能为空" placeholder="请输入用户名" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">密码框</label>
                    <div class="layui-input-block">
                        <input type="password" name="password" lay-verify="required" lay-reqText="密码不能为空" placeholder="请输入密码" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn register" type="button" >注册</button>
                        <button class="layui-btn" lay-submit lay-filter="login">登录</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script>
    //Demo
    layui.use(['form','jquery','layer'], function(){
        var form = layui.form;
        let $ = layui.jquery;
        let layer = layui.layer;

        $(".register").click(e=>{
            layer.open({
                type:2,
                area:["400px","300px"],
                content:"${pageContext.request.contextPath}/register.jsp"
            })
        });

        //监听提交
        form.on('submit(login)', function(data){
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath}/userInfo/login?m=login',
                data:data.field,
                dataType:'json',
                success(rs){
                    if(rs.code == 0){
                        layer.msg(rs.message);
                        location.href="${pageContext.request.contextPath}/index.jsp";
                    }else{
                        layer.msg(rs.message,{icon:5});
                    }
                }
            })
            return false;
        });
    });
</script>
</body>
</html>
