<%--
  Created by IntelliJ IDEA.
  User: 许杰
  Date: 2021/1/14
  Time: 11:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="layui.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/login.css">
    <style>
        body{
            padding-right: 10px;
        }
    </style>
</head>
<body>
<div class="lay-row register">
    <div class="layui-col-md4 layui-col-md-offset4">
        <form class="layui-form">
            <div class="layui-form-item">
                <label class="layui-form-label">用户名:</label>
                <div class="layui-input-block">
                    <input type="text" name="username" lay-verify="required" lay-reqText="用户名不能为空" placeholder="请输入用户名" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">密码:</label>
                <div class="layui-input-block">
                    <input type="password" name="password" lay-verify="required" lay-reqText="密码不能为空" placeholder="请输入密码" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">确认密码</label>
                <div class="layui-input-block">
                    <input type="password" lay-verify="required|passwd" lay-reqText="密码不能为空" placeholder="请输入密码" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="register">注册</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form','jquery','layer'],function(){
        let {form,$,layer} = layui;
        form.verify({
            passwd:function(value,item){
                if ($("input[name=password]").val() != value) {
                    return "俩次密码不一致";
                }
            }
        });
        form.on("submit(register)",function(data){
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath}/userInfo/register?m=register',
                data:data.field,
                dataType:'json',
                success(rs){
                    if(rs.code == 0){
                        let frameIndex = parent.layer.getFrameIndex(window.name);
                        parent.layer.msg(rs.message);
                        parent.layer.close(frameIndex);
                    }else{
                        layer.msg(rs.message,{icon:5});
                    }
                }
            })
            return false;
        })
    });
</script>
</body>
</html>
